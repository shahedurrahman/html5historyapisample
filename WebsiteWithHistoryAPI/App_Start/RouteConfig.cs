﻿using System.Web.Mvc;
using System.Web.Routing;

namespace WebsiteWithHistoryAPI
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //routes.MapRoute(
            //    name: "About",
            //    url: "{action}/{id}",
            //    defaults: new { Controller = "Home", Action = "Index", id = UrlParameter.Optional },
            //    constraints: new { Action = "About|Contact" }
            //);

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
