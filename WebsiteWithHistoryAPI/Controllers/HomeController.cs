﻿using System.Web.Mvc;

namespace WebsiteWithHistoryAPI.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            if (Request.IsAjaxRequest())
                return PartialView("_Index");

            return View();
        }


        // GET: About
        public ActionResult About()
        {
            if (Request.IsAjaxRequest())
                return PartialView("_About");

            return View();
        }

        // GET: Contact
        public ActionResult Contact()
        {
            if (Request.IsAjaxRequest())
                return PartialView("_Contact");

            return View();
        }
    }
}